
Welcome to using DragonBones Pro json to orx ini converter.
Supported DragonBones version: 4.6
Supported orx version: 1.7

This software (jtorx) is under Artistic License 2.0
Copyright (c) 2016, Pyry Veijalainen

Content:
-INFORMATION
-INSTALLATION
-USING JTORX
-FAQ

————————————————————
INFORMATION
————————————————————

This program converts DragonBones Pro json files to orx ini files that are ready to use.
It is simple to use: drag the files into jtorx working directory and run the exe.
The program will automatically create all the graphic and animation objects for you
including all the possible animation links.

This will let you to manage and create all your animations for an object in ORX
with DragonBones Pro as a single DragonBones Pro project.

The prefixes used for objects in the generated ini:
AL_ - (Animation)Link object
AN_ - Animation object
AS_ - AnimationSet object
GR_ - Graphic object

NOTE:

Timeline curves in DragonBones are not supported -> they have no effect.
In order to create smoothing or any other time stretching in animations
you need to manually edit config and add KeyDuration[int] settings.

Property and Priority for animations will need to be manually set if/when needed.

IT IS IMPORTANT TO EXPORT FROM DRAGONBONES IN THE RIGHT FORMAT, CHECK USING JTORX SECTION!!! WRONG FORMATS WILL RESULT IN ERROR.


————————————————————
INSTALLATION
————————————————————

BY COMPILING YOUR OWN

1. In your favourite C programming IDE create 3 files: main.c, jto.c and jto.h
2. Copy paste the code from source_code into the corresponding files.
3. Compile the code.
4. Rename the executable into jtorx or something else with 5 characters or the program will not work.
5. Move the executable into a desired folder.
6. Create a folder named workdr to the same folder where you have the executable.
7. Create a file named license.txt and copy the license text into it.
8. OPTIONAL: Create a file named default_fps.cfg to the same folder where you have the executable.
9. OPTIONAL: Write desired fps as an integer number into the default_fps.cfg and nothing else.


BY ZIP ARCHIVE (MAY NOT WORK ON YOUR SYSTEM - only mac?)

1. Extract the zip folder on your desktop.
2. Move the jtorx folder to somewhere you want.

If the zip method is not working you need to do it by compiling your own executable.


————————————————————
USING JTORX
————————————————————

1. From DragonBones go to export menu.
2. Select Image Sequence.
3. Change Image Type to Texture Atlas.
4. CLICK SETTINGS next to the Texture Atlas and UNTICK Strip Whitespace
5. From Content make sure you are using All Animation.
6. Click finish and after Export Success click confirm.

7. Go to the export folder where you have your .json and .png file.
8. Rename the .json and .png files to something unique, do not change the extensions.
   The config file, texture file in the config ini and animation set are named according
   to the .json file.
9. Copy or move the .json file to the workdr folder of jtorx.
   NOTE: All .json files will be deleted from workdr after conversion.
10. Double click or run the executable file.
11. You have your ready to go .ini files now in your workdr.
12. Move the generated .ini and your .png file into your project resource folders.

DO NOT FORGET TO ADD @file_name.ini@ into your project_name.ini main config file!!!


Additional tips:

You should name your animations inside your project so that they are unique in your game.
Do not use “walk” as an animation name for your every character, use e.g. “HeroWalk” or “zombie_walk”
instead as otherwise the automatically generated ini files will override themselves if you
have multiple characters with same animation names.

Creating animations with multiple fps variations is possible, but you need to manually set
the KeyDefaultDuration for those animations that are not using your default_fps.cfg setting.

The easiest way to setup TopLeft and BottomRight values for your animation (with box BodyPart)
is to begin by setting TopLeft to (0,0,0) and BottomRight to be identical with TextureSize
under [GR_FileName] run once and then start to increase TopLeft values and decrease BottomRight
values.


————————————————————
FAQ
————————————————————

My jtorx is not finding any .json files.
- check folder structure to be identical with the instructions
- you can download the jtorx.zip and replace the executive with
  the one you compiled for the correct file structure

My animation is not working correctly or it is flickering.
- check carefully that you have the correct .json file format instructed in USING JTORX 1-6
- check what fps value you have in the default_fps.cfg

My animation is floating in air.
(I have no clue ATM why this is happening)
- manually adjust your body part size values (TopLeft and BottomRight for a box),
  it is best to start with a body about the size of TextureSize under GR_FileName

My animation is not showing up.
- check that the texture is not too big for your screen, adjust Scale setting of your object
- did you add @file_name.ini@ into your main config project_name.ini?