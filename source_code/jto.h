// This software (jtorx) is under Artistic License 2.0
// Copyright (c) 2016, Pyry Veijalainen


#ifndef JTO_H
#define JTO_H

typedef struct{
    int x1;
    int y1;
    int x2;
    int y2;
    int slot;
    char name[100];
}orxT;

typedef struct{
    orxT **frames;
    unsigned int textures;
    char name[100];
}orxA;

typedef struct{
    orxA **anim_list;
    unsigned int anims;
}orxI;

struct LinkedList{
    char *name;
    struct LinkedList *next;
};

char *get_exe_directory(char *exe);
int get_fps(char *path);
struct LinkedList **get_filenames(char *exe_directory);
void free_linked_list(struct LinkedList **list);
char *read_json(char *exe_directory, char *file_name);
orxI *parse_data(char *source);
void free_orxI(orxI *config);
void create_orxI(orxI *config,char *path, char *file_name, int default_fps);

#endif

