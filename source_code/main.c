// This software (jtorx) is under Artistic License 2.0
// Copyright (c) 2016, Pyry Veijalainen


#include <stdio.h>
#include <stdlib.h>
#include "jto.h"
#include <sys/param.h>

int main(int argc, char** argv) {
    printf("\n---------------------------------------------------------\n");
    printf("jtorx, a DragonBones Pro json to orx ini converter\n");
    printf("This software is under Artistic License 2.0\n");
    printf("Copyright (c) 2016, Pyry Veijalainen\n");
    printf("Version: 1.0\n");
    printf("Watch the magic happen!\n");
    printf("---------------------------------------------------------\n\n");
    char *exe_directory = get_exe_directory(argv[0]);
    
    struct LinkedList **files = get_filenames(exe_directory);
    if (!files) {
        free(exe_directory);
        printf("No json files were found under workdr.\n");
        printf("Terminating program.\n\n");
        return(EXIT_SUCCESS);
    }
    struct LinkedList *file = files[0];
    printf("Handling files:\n");
    
    const int fps = get_fps(exe_directory);

    while (file){
        printf("%s... ",file->name);
        char *text = read_json(exe_directory,file->name);
        orxI *config = parse_data(text);
        free(text);
        create_orxI(config,exe_directory,file->name,fps);
        free_orxI(config);
        printf("OK\n");
        file = file->next;
    }
    printf("\n");
    free_linked_list(files);
    free(exe_directory);
    return (EXIT_SUCCESS);
}

