// This software (jtorx) is under Artistic License 2.0
// Copyright (c) 2016, Pyry Veijalainen

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>
#include <ctype.h>
#include <dirent.h>
#include "jto.h"

char *extend_string(char *string, char newc, unsigned int new_size){
    char *new_string = malloc(new_size*sizeof(char));
    
    if (new_size > 2){
        for (unsigned int i = 0; i < new_size-2;i++){
        new_string[i] = string[i];
        }
    }
        
    new_string[new_size-2] = newc;
    new_string[new_size-1] = '\0';
    
    free(string);
    return new_string;
}

char *strip_path(char *path, int chars){
    char *c = path;
    if (!c) return NULL;
    int size = 0;
    
    while (*c != '\0'){
        size++;
        c++;
    }
    
    char *new_path = (char*)malloc(sizeof(char));
    new_path[0] = '\0';
    
    size -= chars;
    if (size <= 0){
        return new_path;
    }
    
    for (int i = 0; i < size;i++){
        new_path = extend_string(new_path,path[i],i+2);
    }
    
    return new_path;
}

char *combine_strings(char *dest, char *string){
    unsigned int size = 1;
    char *custom_path = (char*)malloc(sizeof(char));
    
    char *c = dest;
    
    while (*c != '\0'){
        size++;
        custom_path = extend_string(custom_path,*c,size);
        c++;
    }
    
    c = string;
    
    while (*c != '\0'){
        size++;
        custom_path = extend_string(custom_path,*c,size);
        c++;
    }
    
    return custom_path;
}

char *get_exe_directory(char *exe){
    char exe_path[PATH_MAX];
    memset(exe_path,'\0',PATH_MAX);
    realpath(exe,exe_path);
    char *path = strip_path(exe_path,5);
    return path;
}

int get_fps(char *path){
    const int default_fps = 25;
    char *cfg = combine_strings(path,"default_fps.cfg");
    char buf[10];
    memset(buf,'\0',sizeof(buf));
    FILE *fp = fopen(cfg,"r");
    free(cfg);
    if (!fp) {
        printf("\nCOULD NOT FIND default_fps.cfg, using default fps of %d\n\n", default_fps);
        return default_fps;
    }
    
    fgets(buf,10,fp);
    fclose(fp);
    
    int result = atoi(buf);
    if (result <= 0) {
        printf("\nERROR READING default_fps.cfg or found bad value, using default fps of %d\n", default_fps);
        printf("Please use an integer value between 0 and 200.\n\n");
        return default_fps;
    }
    else if (result > 199){
        printf("\nFPS TOO BIG! USE VALUE UNDER 200, using default fps of %d\n\n", default_fps);
        return default_fps;
    }
    
    return result;
}

struct LinkedList **get_filenames(char *exe_directory){
    char *full_path = combine_strings(exe_directory,"workdr");
    DIR *dp = opendir(full_path);
    if (!dp) {
        free(full_path);
        return NULL;
    }
    
    struct dirent *file = readdir(dp);
    if (!file){
        free(full_path);
        closedir(dp);
        return NULL;
    }
    
    unsigned int file_amount = 0;
    
    while (file){
        if (strstr(file->d_name,".json")) file_amount++;
        file = readdir(dp);
    }
    
    if (file_amount == 0){
        free(full_path);
        closedir(dp);
        return NULL;
    }
    
    rewinddir(dp);
    struct LinkedList **name_list = (struct LinkedList**)malloc(file_amount*sizeof(struct LinkedList*));
    
    unsigned int i = 0;
    file = readdir(dp);
    struct LinkedList *prev = NULL;
    
    while (file){
        if (strstr(file->d_name,".json")){
            name_list[i] = (struct LinkedList*)malloc(sizeof(struct LinkedList));
            name_list[i]->name = strdup(file->d_name);
            name_list[i]->next = NULL;
            
            if (i > 0){
                prev->next = name_list[i];
            }
            
            prev = name_list[i];
            i++;
        }
        file = readdir(dp);
    }
    closedir(dp);
    free(full_path);
    return name_list;
}

void free_linked_list(struct LinkedList **list){
    if (!list) return;
    
    struct LinkedList *prev = list[0];
    struct LinkedList *next = list[0]->next;
    
    while (1){
        free(prev->name);
        free(prev);
        
        prev = next;
        
        if (prev){
            next = prev->next;
        }
        else {
            free(list);
            break;
        }
    }
}

char *read_json(char *exe_directory,char *file_name){
    
    char *file_path = combine_strings("workdr/",file_name);
    char *full_path = combine_strings(exe_directory,file_path);
    free(file_path);
    
    char c;
    FILE *f = fopen(full_path,"r");
    
    if (!f) return NULL;
    
    unsigned int size = 1;
    char *content = (char*)malloc(sizeof(char));
    content[0] = '\0';
    
    while (1){
        c = fgetc(f);
        if (feof(f)) break;
        size++;
        content = extend_string(content,c,size);
    }
    
    fclose(f);
    free(full_path);
    return content;
}

char *find_next(char *c){
    while (*c != '"' && *c != '\0'){
        c++;
    }
    
    if (*c == '\0') return NULL;
    c++;
    return c;
}

char *get_string(char *dest,char *c){
    unsigned int i = 0;
    while (*c != '"' && *c != '\0'){
       dest[i] = *c;
       c++;
       i++;
       if (i == 99) break;
    }
    dest[i] = '\0';
    if (*c == '\0') return NULL;
    c++;
    return c;
}

char *get_num(int *storage, char *c){
    c++;
    char dest[100];
    
    unsigned int i = 0;
    while (*c != ',' && *c != '\0'){
        dest[i] = *c;
        c++;
        i++;
        if (i == 99) break;
    }
    dest[i] = '\0';
    *storage = atoi(dest);
    if (*c == '\0') return NULL;
    c++;
    return c;
}

int get_slot(char *name){
    char buf[30];
    char *c = name;
    unsigned int i = 0;
    
    while (*c != '_'){
        c++;
    }
    c++;
    
    while (*c != '\0'){
        buf[i] = *c;
        c++;
        i++;
        if (i == 29) break;
    }
    buf[i] = '\0';
    
    return atoi(buf);
}

void strip_name(char *dest,char *name){
    char *c = name;
    int i = 0;
    while (*c != '_') {
        dest[i] = *c;
        c++;
        i++;
        if (i == 99) break;
    }
    dest[i] = '\0';
}

void copy_texture(orxT *storage, orxT t){
    storage->slot = t.slot;
    storage->x1 = t.x1;
    storage->x2 = t.x2;
    storage->y1 = t.y1;
    storage->y2 = t.y2;
    strcpy(storage->name,t.name);
}

orxI *parse_data(char *source){
    char buf[100];
    char *c = source;
    int n = 0;
    
    char pick_name = 0;
    unsigned int blocks = 0;
    
    orxI *config = (orxI*)malloc(sizeof(orxI));
    config->anims = 0;
    config->anim_list = (orxA**)malloc(sizeof(orxA*));
    
    orxT tex;
    
    while (1){
        c = find_next(c);
        if (!c) break;
        
        c = get_string(buf,c);
        if (!c) break;
        
        if (strcmp(buf,"x")==0){
            c = get_num(&n,c);
            tex.x1 = n;
            blocks++;
        }
        else if (strcmp(buf,"y")==0){
            c = get_num(&n,c);
            tex.y1 = n;
            blocks++;
        }
        else if (strcmp(buf,"width")==0){
            c = get_num(&n,c);
            tex.x2 = tex.x1+n;
            blocks++;
        }
        else if (strcmp(buf,"height")==0){
            c = get_num(&n,c);
            tex.y2 = tex.y1+n;
            blocks++;
        }
        else if (strcmp(buf,"name")==0){
            pick_name = 1;
        }
        else if (pick_name){
            pick_name = 0;
            memset(tex.name,'\0',sizeof(tex.name));
            strcpy(tex.name,buf);
            tex.slot = get_slot(buf);
            blocks++;
        }
        
    
        if (blocks == 5){
            blocks = 0;
            char found = 0;
            unsigned int index = 0;
            
            for (unsigned int i = 0; i < config->anims; i++){
                if (strstr(tex.name,config->anim_list[i]->name)){
                    found = 1;
                    break;
                }
                index++;
            }
            
            if (!found){
                index = config->anims;
                config->anims++;
                config->anim_list = (orxA**)realloc(config->anim_list,config->anims*sizeof(orxA*));
                config->anim_list[index] = (orxA*)malloc(sizeof(orxA));
            }
            
            orxA *anim = config->anim_list[index];
            
            if (!found){
                char name[100];
                strip_name(name,tex.name);
                memset(anim->name,'\0',sizeof(anim->name));
                strcpy(anim->name,name);
                anim->frames = (orxT**)malloc(sizeof(orxT*));
                anim->textures = 1;
            }
            
            if (tex.slot+1 > anim->textures){
                anim->textures = tex.slot+1;
                anim->frames = (orxT**)realloc(anim->frames,anim->textures*sizeof(orxT*));
            }
            
            anim->frames[tex.slot] = (orxT*)malloc(sizeof(orxT));
            copy_texture(anim->frames[tex.slot],tex);
            
        }
        
        if(!c) break;
    }
    
    return config;
}


void free_orxI(orxI *config){
    for (unsigned int i = 0; i < config->anims; i++){
        for (unsigned int a = 0; a < config->anim_list[i]->textures;a++){
            free(config->anim_list[i]->frames[a]);
        }
        
        free(config->anim_list[i]->frames);
        free(config->anim_list[i]);
    }
    free(config->anim_list);
    free(config);
}

void create_orxI(orxI *config,char *path, char *file_name, int default_fps){
    char *fn;
    fn = strip_path(file_name,5);
    char *workdr = combine_strings(path,"workdr/");
    char *temp = combine_strings(workdr,fn);
    char *new_file_path = combine_strings(temp,".ini");
    FILE *fp = fopen(new_file_path,"w");
    free(new_file_path);
    free(temp);
    if (!fp){
        printf("Could not create file %s.ini in workdr! Printing result.\n\n",fn);
        fp = stdout;
        printf("%s.ini\n\n",fn);
    }
    
    float dkd = 1/(float)default_fps;
    
    orxT *tex = config->anim_list[0]->frames[0];
    int width = tex->x2 - tex->x1;
    int height = tex->y2 - tex->y1;
    
    fprintf(fp,"[GR_%s]\n",fn);
    fprintf(fp,"Texture = %s.png\n",fn);
    fprintf(fp,"TextureSize = (%d,%d,0)\n\n",width,height);
    
    fprintf(fp,"[AS_%s]\n",fn);
    fprintf(fp,"AnimationList = ");
    for (unsigned int i = 0; i < config->anims;i++){
        fprintf(fp,"AN_%s",config->anim_list[i]->name);
        
        if (i == config->anims-1) {
            fprintf(fp,"\n");
        }
        else if (i+1 % 4 == 0){
            fprintf(fp," #\n\t   ");
        }
        else {
            fprintf(fp," # ");
        }
    }
    
    fprintf(fp,"LinkList = ");
    unsigned int links = 0;
    for (unsigned int i = 0; i < config->anims;i++){
        for (unsigned int a = 0; a < config->anims;a++){
            fprintf(fp,"AL_%s2%s",config->anim_list[i]->name,config->anim_list[a]->name);
            links++;
            if (i == config->anims-1 && a == config->anims-1){
                fprintf(fp,"\n\n");
            }
            else if (links % 3 == 0){
                fprintf(fp," #\n\t   ");
            }
            else {
                fprintf(fp," # ");
            }
        }
    }
    
    for (unsigned int i = 0; i < config->anims;i++){
        for (unsigned int a = 0; a < config->anims;a++){
            fprintf(fp,"[AL_%s2%s]\n",config->anim_list[i]->name,config->anim_list[a]->name);
            fprintf(fp,"Source = AN_%s\n",config->anim_list[i]->name);
            fprintf(fp,"Destination = AN_%s\n\n",config->anim_list[a]->name);
        }
    }
    
    for (unsigned int i = 0; i < config->anims;i++){
        fprintf(fp,"[AN_%s]\n",config->anim_list[i]->name);
        fprintf(fp,"DefaultKeyDuration = %.2f\n",dkd);
        for (unsigned int a = 0; a < config->anim_list[i]->textures;a++){
            fprintf(fp,"KeyData%o = GR_%s\n",a+1,config->anim_list[i]->frames[a]->name);
        }
        fprintf(fp,"\n");
    }
    
    for (unsigned int i = 0; i < config->anims; i++){
        for (unsigned int a = 0; a < config->anim_list[i]->textures;a++){
            orxT *t = config->anim_list[i]->frames[a];
            int x = t->x1;
            int y = t->y1;
            
            fprintf(fp,"[GR_%s@GR_%s]\n",config->anim_list[i]->frames[a]->name,fn);
            fprintf(fp,"TextureCorner = (%d,%d,0)\n",x,y);
            
            x = t->x2 - t->x1;
            y = t->y2 - t->y1;
            
            if (x != width || y != height){
                fprintf(fp,"TextureSize = (%d,%d,0)\n\n",x,y);
            }
            else {
                fprintf(fp,"\n");
            }
        }
    }
    if (fp != stdout) {
        fclose(fp);
    }
    char *old_file = combine_strings(workdr,file_name);
    remove(old_file);
    free(old_file);
    free(workdr);
    free(fn);
}

